package com.example.auth.basic.security.config;

import com.example.auth.basic.security.filter.BasicAuthFilter;
import com.example.auth.basic.security.filter.JwtRequestFilter;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public BasicAuthFilter basicAuthFilter(AuthenticationManager authenticationManager){
        BasicAuthFilter filter
                = new BasicAuthFilter(new AntPathRequestMatcher("/**")); // ant matcher tells filter
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

    public JwtRequestFilter jwtRequestFilter(AuthenticationManager authenticationManager){
        JwtRequestFilter filter
                = new JwtRequestFilter(new AntPathRequestMatcher("/**")); // ant matcher tells filter
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO: remove below comments once we get aware about the reason for disabling csrf
        // this is to recover 403 in case of PUT, POST, PATCH, DELETE
        // refer - http.csrf().disable();
        // Is CSRF related to Session ? as I can see the
        // Exception as InvalidCsrfTokenException
        // with message Could not verify the provided CSRF token because your session was not found.
        // To reproduce InvalidCsrfTokenException, enable csrf
        // then add breakpoint in CsrfFilter( of Spring security filter chain) -> doFilterInternal(..)
        http.csrf().disable();

        // if we keep below line active then login via
        // default login page will not work
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(
                jwtRequestFilter(authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
        ;

        http.addFilterBefore(
                basicAuthFilter(authenticationManager()),
                UsernamePasswordAuthenticationFilter.class)
        ;

        //  authenticate all other apis
        http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });

        // use of belo commented line
        // http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.formLogin();
        http.httpBasic();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}admin").roles("Admin");
    }
}
