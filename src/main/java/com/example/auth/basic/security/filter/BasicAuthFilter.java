package com.example.auth.basic.security.filter;

import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BasicAuthFilter extends AbstractAuthenticationProcessingFilter {

    public BasicAuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    // TODO:
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String username = getUsername(httpServletRequest);
        String password = getPassword(httpServletRequest);

        if(username == null ){
            username = "admin" ;
        }
        if(password == null){
            password ="admin";
        }
        // log the user
        //log.debug("Authenticating User::::::::::::::::::::::::::::::::::::::::::::: " + userName);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        return this.getAuthenticationManager().authenticate(token);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        this.doFilterInternal((HttpServletRequest)request, (HttpServletResponse)response, chain);
    }


    protected String getPassword(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader("apiSecret"); // pass password in ths header form test and postman
    }

    protected String getUsername(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getHeader("apiKey"); // pass username in ths header form test and postman
    }

    private void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null){
            Authentication authenticationResult = this.attemptAuthentication(request, response);
            if (authenticationResult == null) {
                chain.doFilter(request, response);
            }else{
                SecurityContextHolder.getContext().setAuthentication(authenticationResult);
                chain.doFilter(request, response);
            }
        }else{
            chain.doFilter(request, response);
        }
    }
}
