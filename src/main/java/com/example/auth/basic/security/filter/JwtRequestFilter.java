package com.example.auth.basic.security.filter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtRequestFilter  extends AbstractAuthenticationProcessingFilter {

    public JwtRequestFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    // TODO:
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String jwtToken = getJwtToken(httpServletRequest);
        boolean isValidJwtToken = true;
        if(jwtToken == null){
            jwtToken = "";
            isValidJwtToken = false;
        }
        // authenticate jwt token
        if(isValidJwtToken){
            // passing all the value as null to mark authentication flag as true
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    null,
                    null,
                    null);
            return token;
        }else{
            return null;
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        this.doFilterInternal((HttpServletRequest)request, (HttpServletResponse)response, chain);
    }

    private void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Authentication authenticationResult = this.attemptAuthentication(request, response);
        if (authenticationResult == null) {
            chain.doFilter(request, response);
        }else{
            SecurityContextHolder.getContext().setAuthentication(authenticationResult);
            chain.doFilter(request, response);
        }
    }

    protected String getJwtToken(HttpServletRequest httpServletRequest) {
        String jwtToken = httpServletRequest.getHeader("Authorization");
        if(jwtToken == null){
            return null;
        }
        if(!jwtToken.startsWith("Bearer")){
            return null;
        }
        return jwtToken.substring(7);

    }
}
