package com.example.auth.basic.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/secure")
public class SecureResourceController {

    @GetMapping
    public String getApi(){
        return  "ok from SecureResourceController -> getApi";
    }
}
